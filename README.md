# Materials and instructions

This repository contains all materials and instructions that will be useful for you during Pivorak Ruby Summer Course 2019

## Table of Contents

### Pre-requirements

Please check the next materials before you'll proceed with your first homework:
- [Pivorak Summer Course Rules](https://docs.google.com/document/d/1uJWZXLPANSxBX2XAQcLRYAYkTz5bpBIXfhQaV3avzyo/edit)
- [Register Gitlab account](https://gitlab.com/users/sign_up)
- [How To Ask Questions The Smart Way](http://www.catb.org/~esr/faqs/smart-questions.html)
- [How to Write a Git Commit Message](https://chris.beams.io/posts/git-commit/)
- [How to Name a Branch](https://gist.github.com/digitaljhelms/4287848)
- [How to Open Correct Merge Request: VIDEO](https://youtu.be/YVWWx1BVVgQ)
- [How to keep your fork up to date with its origin](https://about.gitlab.com/2016/12/01/how-to-keep-your-fork-up-to-date-with-its-origin/)

### Lectures agenda
| Lecture slides | Homework | Additional materials |
|-----------------|:-------------|:---------------:|
| [Setup ENV](https://docs.google.com/presentation/d/1NgelqJPN9zTP3p5VNu4DKsXFF5WMb2-0rY317BJmA7A/edit?ts=5cf37994#slide=id.p1) | N/A |  pending |
| [Ruby Basics](https://drive.google.com/file/d/1vvzdv5nFehBQdRDUU5yZUClKfE2TmU63/view?usp=sharing) | [link](https://gitlab.com/pivorak-rsc-2019/homeworks/homework_ruby_basics) | [Homework aftermath](https://docs.google.com/presentation/d/1PNcIjNH8Pjm0jc9pGmX2I6VLEx2S9_bwwi8nqlo9OTw/edit#slide=id.g5b4b5aa47d_4_1) |
| [Ruby OOP](https://drive.google.com/open?id=1UEqXYCcTzaLPM4cYzMUc4NQUzIjuLE9o) | [Catalog app](https://gitlab.com/pivorak-rsc-2019/homeworks/ruby-oop?nav_source=navbar) | N/A |
| [Testing](https://drive.google.com/open?id=1ykxhuY1Ot9leCHnKu7PcyqDPpi5u0WMZQ3FBlCYCvMc) | [GitLab](https://gitlab.com/pivorak-rsc-2019/homeworks/testing) | [Review](https://drive.google.com/open?id=1SlCgeVTE92iBG2KRwa_0vOXCPEw9BZINOzpeg7xFzYo) |
| Rails Console | pending | [Screencast Rails Testing](https://www.youtube.com/watch?v=Ka2alUFcafM) |
| [Rails HTTP](https://drive.google.com/file/d/1-kqUV4n_bNnnC4BaeoCAqTELHbpK6bWD) | [Homework](https://gitlab.com/pivorak-rsc-2019/homeworks/rails-http) | Links on slides |
| [Rails API](https://drive.google.com/open?id=1mgeyeHzNfWpW56Ih-RJpWHXMdyzAHuCDbj03b4iauHo) | pending | pending |
| [Design Patterns](https://docs.google.com/presentation/d/1EUrtHceMQXU7GSvNDDDsz_9dLmW77ZDFoCulsZFzYK4/edit?usp=sharing) | pending | pending |

### How to work with homeworks via Gitlab
In order to simplify process of Code Review we'll use gitlab groups.
Each of you received your own unique [gitlab group](https://docs.gitlab.com/ee/user/group/) for RSC 2019 homeworks.

In order to start work on your homework please open specific repository from [homeworks](https://gitlab.com/pivorak-rsc-2019/homeworks) group:
![Open homeworks folder](https://files.g3d.codes/Screen-Shot-2019-06-03-19-33-25-1559579683.png)

When you will open your repository please fork it into your private homework group:
![Click fork button](https://files.g3d.codes/Screen-Shot-2019-06-03-19-38-09-1559579922.png)
![Select your homework gitlab group](https://files.g3d.codes/Screen-Shot-2019-06-03-19-40-40-1559580097.png)

After fork feel free to work on your homework!

Don't implement your solution on master branch in your fork. Checkout to new one - name does not matter.
When your solution is ready create Merge request against master branch in your fork and invite lecturer to review your Merge request
[How to create correct Merge Request](https://youtu.be/YVWWx1BVVgQ)

The faster you do this - the more time you will have to improve your work and get higher score

In case if something will change on origin branch. To update your fork - follow up instructions in this article:
[How to keep your fork up to date with its origin](https://about.gitlab.com/2016/12/01/how-to-keep-your-fork-up-to-date-with-its-origin/)